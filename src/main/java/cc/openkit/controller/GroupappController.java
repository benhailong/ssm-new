package cc.openkit.controller;

import cc.openkit.model.Groupapp;
import cc.openkit.service.GroupappService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequestMapping("group")
@Controller
public class GroupappController {

    @Autowired
    private GroupappService groupService;

    // 查询所有
    @RequestMapping("/getAll")
    @ResponseBody
    public Map<String,Object> getAll(){
        Map<String,Object> map =new HashMap<String,Object>();
        List<Groupapp> groups = groupService.queryAll();
        map.put("result", groups);
        return map;
    }

    // 查根据主键查询
    @RequestMapping(value = "/getAllByPK", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> getAllByPK(HttpServletRequest request) throws Exception{

        String gid = request.getParameter("gid");

        System.out.println(gid);

        Map<String,Object> map =new HashMap<String,Object>();
        Groupapp groups = groupService.queryByUUID(gid);
        map.put("result", groups);
        return map;
    }

    // 查根据主键查询
    @RequestMapping(value = "/queryListByWhere", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> queryListByWhere(HttpServletRequest request) throws Exception{

        Map<String,Object> map =new HashMap<String,Object>();

        Groupapp groupapp = new Groupapp();
        groupapp.setGroupname("会计");
        groupapp.setGroupid("75d8f42a-c842-4417-b923-eb50ad38d6b2");

        List<Groupapp> groups = groupService.queryListByWhere(groupapp);
        map.put("result", groups);
        return map;
    }






}
