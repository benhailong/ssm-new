package cc.openkit.service;

import cc.openkit.dao.GroupappMapper;
import cc.openkit.model.Groupapp;
import cc.openkit.service.common.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GroupappService extends BaseService<Groupapp>{

    @Autowired
    private GroupappMapper groupappMapper;
}
